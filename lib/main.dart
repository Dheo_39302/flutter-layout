import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo Home Page ',
      home: Home('Strawberry Pavlova Recipe'),
    );
  }

  Widget Home(String title) {
    final titleText = Container(
      height: 30,
      width: 500,
    child: Card(
        color: Colors.lightBlue,
      child: Center(
      child: Text(
        'Strawberry Pavlova',
        style: TextStyle(
        fontWeight: FontWeight.w800,
        letterSpacing: 0.5,
        fontSize: 20,
          ),
        ),
      ),
    ),
    );

    final description = Container(
      height: 65,
      width: 500,
    child: Card(
        color: Colors.lightBlue,
        child: Center(
        child: Text(
          'Pavlova is a meringue-based dessert named after the Russian ballerina '
              'Anna Pavlova. Pavlova features a crisp crust and soft, light inside, '
              'topped with fruit and whipped cream.',
          style: TextStyle(
          fontWeight: FontWeight.w800,
            letterSpacing: 0.5,
            fontSize: 15,
            ),
          ),
        ),
      ),
    );

    var stars = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.star, color: Colors.black),
        Icon(Icons.star, color: Colors.black),
        Icon(Icons.star, color: Colors.black),
        Icon(Icons.star, color: Colors.black),
        Icon(Icons.star, color: Colors.black),
      ],
    );

    final ratings = Card(
      color: Colors.lightBlue,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          stars,
          Text(
            '170 Reviews',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w800,
              fontFamily: 'Roboto',
              letterSpacing: 0.5,
              fontSize: 15,
            ),
          ),
        ],
      ),
      );

    final descTextStyle = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w800,
      fontFamily: 'Roboto',
      letterSpacing: 0.5,
      fontSize: 10,
      height: 2,
    );

    final iconList = DefaultTextStyle.merge(
      style: descTextStyle,
      child: Card(
        color: Colors.lightBlue,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Icon(Icons.kitchen, color: Colors.green[500]),
                Text('PREP:'),
                Text('25 min'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.timer, color: Colors.green[500]),
                Text('COOK:'),
                Text('1 hr'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.restaurant, color: Colors.green[500]),
                Text('FEEDS:'),
                Text('4-6'),
              ],
            ),
          ],
        ),
      ),
      );

    final LeftColumn = Container(
      padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
      child: Column(
        children: [
          titleText,
          description,
          ratings,
          iconList,
        ],
      ),
    );

    final image = Image.asset(
      'images/pavlova.jpg',
      height: 250,
      fit: BoxFit.cover,
    );

    return Scaffold(
      appBar: AppBar(
      title: Text(title),
          ),
      backgroundColor: Colors.greenAccent,
      body: Center(
        child: Container(
          margin: EdgeInsets.fromLTRB(10, 100, 10, 120),
          height: 600,
        child: FittedBox(
        child: Card(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [Container(
            width: 500,
            child:
            LeftColumn,
          ),
            image,
          ],
         ),
        ),
        ),
       ),
      ),
    );
  }
}

